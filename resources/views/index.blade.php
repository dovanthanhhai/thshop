@extends('frontend.layout.layout')
@section('title',$title ?? '')
@section('content')
<main id="main">
    <div class="container">

        <!--MAIN SLIDE-->
        <div class="wrap-main-slide">
           @include('frontend.product.slider')
        </div>

        <div class="wrap-show-advance-info-box style-1">
            @include('frontend.product.promotion')
        </div>
        
        <div class="wrap-show-advance-info-box style-1">
            @include('frontend.product.book')
        </div>

        <div class="wrap-show-advance-info-box style-1 has-countdown">
           @include('frontend.product.featuredProducts')
        </div>

        <div class="wrap-show-advance-info-box style-1">
            @include('frontend.product.latesProduct')
        </div>

        <div class="wrap-show-advance-info-box style-1">
            @include('frontend.product.stationery')
        </div>

    </div>
</main>
@endsection