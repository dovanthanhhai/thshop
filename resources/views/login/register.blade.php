@extends('frontend.layout.layout')
@section('title',$title ?? '')  
@section('content')
    <!--main area-->
    <main id="main" class="main-site left-sidebar">

        <div class="container">
            <div class="wrap-breadcrumb">
                <ul>
                    <li class="item-link"><a href="#" class="link">home</a></li>
                    <li class="item-link"><span>Register</span></li>
                </ul>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 col-md-offset-3">							
                    <div class=" main-content-area">
                        <div class="wrap-login-item ">
                            <div class="register-form form-item">
                                <form class="form-stl" action="{{ route('user.store') }}" name="frm-login" method="post"  >
                                    @csrf
                                    <div class="wrap-title">
                                        <h3 class="form-title">Create an account</h3>
                                    </div>	
                                    <div class="row">
                                        <div class="wrap-input col-md-12">
                                            <label for="frm-reg-lname">Username</label>
                                            <input type="text" id="frm-reg-lname" name="username" required placeholder="Username">
                                        </div>
                                        <div class="wrap-input col-md-6">
                                            <label for="frm-reg-lname">Password</label>
                                            <input type="password" id="password" name="password" required placeholder="********">
                                        </div>
                                        <div class="wrap-input col-md-6">
                                            <label for="frm-reg-lname">Confirm Password</label>
                                            <input type="password" id="confirm" name="confirm" required placeholder="********">
                                        </div>
                                        <div class="wrap-input col-md-6">
                                            <label for="frm-reg-lname">Name</label>
                                            <input type="text" id="frm-reg-lname" name="name" required placeholder="Name">
                                        </div>
                                        <div class="wrap-input col-md-6">
                                            <label for="frm-reg-lname">Gender</label>
                                            <input type="text" id="frm-reg-lname" name="gender" required placeholder="Gender">
                                        </div>
                                        <div class="wrap-input col-md-6">
                                            <label for="frm-reg-lname">Address</label>
                                            <input type="text" id="frm-reg-lname" name="address" required placeholder="Address">
                                        </div>
                                        <div class="wrap-input col-md-6">
                                            <label for="frm-reg-lname">Phone</label>
                                            <input type="text" id="frm-reg-lname" name="phone" required placeholder="Phone">
                                        </div>
                                        <div class="wrap-input col-md-6">
                                            <label for="frm-reg-lname">Email</label>
                                            <input type="email" id="frm-reg-lname" name="email" required placeholder="Email">
                                        </div>
                                        <div class="col-md-8">
                                        </div>
                                        <div class="col-md-4" style="text-align: center">
                                            <input type="submit" class="btn btn-sign" value="Register" name="register">
                                        </div>
                                    </div>
                                </form>
                            </div>											
                        </div>
                    </div><!--end main products area-->		
                </div>
            </div>
        </div>
    </main>
@endsection
