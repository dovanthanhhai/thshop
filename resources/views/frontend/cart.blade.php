@extends('frontend.layout.layout')
@section('title', $title ?? '')
@section('content')
    <!--main area-->
	<main id="main" class="main-site">

		<div class="container">

			<div class="wrap-breadcrumb">
				<ul>
					{{-- <li class="item-link"><a href="{{ route('index') }}" class="link">home</a></li> --}}
					<li class="item-link"><span>Cart</span></li>
				</ul>
			</div>
			<div class=" main-content-area">
				<div class="wrap-iten-in-cart">
					<h3 class="box-title">Products Name</h3>
                    @php
                        $total = 0; 
                    @endphp
					@if (!empty($cart))
                        <ul class="products-cart">
                            @foreach ($cart as $item)
                              @php
                                $total_pro = $item->quantity * $item->price;
                                    $total += $item->quantity * $item->price;
                              @endphp
                                <li class="pr-cart-item">
                                    <div class="product-image">
                                        <figure><img src="{{ asset('products/' . $item->image) }}" alt="{{ $item->name }}"></figure>
                                    </div>
                                    <div class="product-name">
                                        <a class="link-to-product" href="{{ route('product-details', ['any' => $item->url, 'id' => $item->id]) }}">{{ $item->name }}</a>
                                    </div>
                                    <div class="price-field produtc-price"><p class="price">${{ number_format($item->price) }}</p></div>
                                    <div class="quantity">
                                        <div class="quantity-input" data-id="{{ $item->id }}">
                                            <input type="text" name="product-quantity" class="product-quantity" value="{{$item->quantity}}" data-max="120" pattern="[0-9]*" >
                                            <input type="hidden" name="product-quantity" class="product-price" value="{{$item->price}}" data-max="120" pattern="[0-9]*" >		
                                            <input type="hidden" value="{{ $item->id }}" class="productId">
                                            <input type="hidden" class="total-pro"  value="{{$total_pro}}">						
                                            <a class="btn btn-increase" href="#"></a>
                                            <a class="btn btn-reduce" href="#"></a>
                                        </div>
                                    </div>
                                    <div class="price-field sub-total"><p class="price"><span>$</span><span class="value">{{ number_format($total_pro) }}</span> </p></div>
                                    <div class="delete">
                                       <a href="#" class="btn btn-delete-item" title="" data-id="{{ $item->id }}" >
                                          <span>Delete from your cart</span>
                                          <i class="fa fa-times-circle" aria-hidden="true"></i>
                                       </a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
					@endif
				</div>
				<div class="summary">
					<div class="order-summary">
						<h4 class="title-box">Order Summary</h4>
                        @if (!empty($cart_number))
                            <p class="summary-info"><span class="title">Subtotal</span><b class="index">
                                <span>$</span><span class="total"> {{ number_format($total) }}</span></b>
                            </p>
                            <p class="summary-info"><span class="title">Shipping</span><b class="index">Free Shipping</b></p>
                        @endif
					</div>
					<div class="checkout-info">
                        @if (!empty($cart_number))
						    <a class="btn btn-checkout" href="{{ route('order-create') }}">Check out</a>
                        @endif
						<a class="link-to-shop" href="{{ route('index') }}">Continue Shopping<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
					</div>
				</div>
			</div><!--end main content area-->
		</div><!--end container-->
	</main>
@endsection