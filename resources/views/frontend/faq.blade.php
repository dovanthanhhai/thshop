@extends('frontend.layout.layout')
@section('title',$title ?? '')
@section('content')
    <!--main area-->
	<main id="main" class="main-site">

		<div class="container">

			<div class="wrap-breadcrumb">
				<ul>
					<li class="item-link"><a href="#" class="link">home</a></li>
					<li class="item-link"><span>FAQ</span></li>
				</ul>
			</div>
		</div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <div class="aboutus-info style-small-left">
                <b class="box-title">Your Problem</b>
                <div class="list-showups">
                    <label>
                        <input type="radio" class="hidden" name="showup" id="shoup1" value="shoup1" checked="checked">
                        <span class="check-box"></span>
                        <span class='function-name'>How long is the store open? </span>
                        <span class="desc">Our store is always open to serve you </span>
                    </label>
                    <label>
                        <input type="radio" class="hidden" name="showup" id="shoup2" value="shoup2">
                        <span class="check-box"></span>
                        <span class='function-name'>How to place an order online </span>
                        <span class="desc">To order products, please follow these steps: </span>
                        <span style="padding: 5%" class="desc">1.Select the product you want to buy.</span>
                        <span style="padding: 5%" class="desc">2.Choose the number of products you need.</span>
                        <span style="padding: 5%" class="desc">3.Click the "Add to cart" button</span>
                        <span style="padding: 5%" class="desc">4.Add to your cart and click "check out"</label>
                    <label>
                        <input type="radio" class="hidden" name="showup" id="shoup3" value="shoup3">
                        <span class="check-box"></span>
                        <span class='function-name'>In which cases is product return applicable? </span>
                        <span class="desc">For any defective product we will return it to the customer free of charge.</span>
                    </label>
                    <label>
                        <input type="radio" class="hidden" name="showup" id="shoup4" value="shoup4">
                        <span class="check-box"></span>
                        <span class='function-name'>I received a missing item, what should I do?</span>
                        <span class="desc">Once a missing order is confirmed, 
                            we will not charge for the missing products or the product will be replaced at a later date. </span>
                    </label>
                    <label>
                        <input type="radio" class="hidden" name="showup" id="shoup3" value="shoup3">
                        <span class="check-box"></span>
                        <span class='function-name'>Is the product received the same as the picture and information on the website? </span>
                        <span class="desc">We guarantee that the product you receive will be exactly the same as the products displayed on the website. </span>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-6">
            <div class="aboutus-info style-small-left">
                <b class="box-title">Other questions</b>
            </div>
        </div>
		<!--end container-->
	</main>
	<!--main area-->
@endsection