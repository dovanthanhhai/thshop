@extends('frontend.layout.layout')
@section('title',$title ?? '')
@section('content')
    <!--main area-->
	<main id="main" class="main-site">

		<div class="container">

			<div class="wrap-breadcrumb">
				<ul>
					<li class="item-link"><a href="#" class="link">home</a></li>
					<li class="item-link"><span>About us</span></li>
				</ul>
			</div>
		</div>
		<div class="container">
				<div class="our-team-info">
					<h4 class="title-box">Our teams</h4>
					<div class="our-staff">
						<div 
							class="slide-carousel owl-carousel style-nav-1 equal-container" 
							data-items="5" 
							data-loop="false" 
							data-nav="true" 
							data-dots="false"
							data-margin="30"
							data-responsive='{"0":{"items":"1"},"480":{"items":"2"},"768":{"items":"3"},"992":{"items":"3"},"1200":{"items":"4"}}' >

							<div class="team-member equal-elem">
								<div class="media">
									<a href="#" title="THIEN">
										<figure><img src="{{ asset('assets/images/3.png')}}" alt="THIEN"></figure>
									</a>
								</div>
								<div class="info">
									<b class="name">DAO THANH THIEN</b>
									<span class="title">LEADER</span>
									<p class="desc">Student1279123</p>
								</div>
							</div>

							<div class="team-member equal-elem">
								<div class="media">
									<a href="#" title="THAO">
										<figure><img src="{{ asset('assets/images/1.png')}}" alt="LUCIA"></figure>
									</a>
								</div>
								<div class="info">
									<b class="name">HOANG VAN THAO</b>
									<span class="title">MEMBER</span>
									<p class="desc">Student1279126</p>
								</div>
							</div>

							<div class="team-member equal-elem">
								<div class="media">
									<a href="#" title="ANH">
										<figure><img src="{{ asset('assets/images/2.png')}}" alt="NANA"></figure>
									</a>
								</div>
								<div class="info">
									<b class="name">VU QUY THIEN ANH </b>
									<span class="title">MEMBER</span>
									<p class="desc">Student1279127</p>
								</div>
							</div>

							<div class="team-member equal-elem">
								<div class="media">
									<a href="#" title="HAI">
										<figure><img src="{{ asset('assets/images/4.png')}}" alt="BRAUM"></figure>
									</a>
								</div>
								<div class="info">
									<b class="name">DO VAN THANH HAI </b>
									<span class="title">Member</span>
									<p class="desc">Student1279</p>
								</div>
							</div>
						</div>

							</div>

						</div>

					</div>

				</div>
			<!-- </div> -->
			

		</div><!--end container-->

	</main>
	<!--main area-->
@endsection