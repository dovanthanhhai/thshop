@extends('frontend.layout.layout')
@section('title',$title ?? '')
@section('content')



    <!--main area-->
	<main id="main" class="main-site left-sidebar">
		<div class="container">
			<div class="wrap-breadcrumb">
				<ul>
					<li class="item-link"><a href="{{ route('index') }}" class="link">home</a></li>
					<li class="item-link"><span>Contact us</span></li>
				</ul>
			</div>
			<form action="{{ route('contact.store') }}" method="post">
				@csrf
				<div class="contact">
					<div class="col-md-6 contact-item">
						<label for="name">Name:</label><br>
						<input type="text" class="contact-input" name="name" required>
					</div>
					<div class="col-md-6 contact-item">
						<label for="name">Telephone:</label><br>
						<input type="text" class="contact-input" name="phone" required>
					</div>
					<div class="col-md-6 contact-item">
						<label for="name">Email:</label><br>
						<input type="email" class="contact-input" name="email" required>
					</div>
					<div class="col-md-6 contact-item">
						<label for="name">Subject:</label><br>
						<input type="text" class="contact-input" name="subject">
					</div>
					<div class="col-md-6 contact-item">
						<label for="message">Message:</label>
						<br>
						<textarea id="message" class="messenge-item" name="message" required></textarea>
					</div>
					<div class="col-md-6 contact-item">
						<label for="name">Map:</label><br>
						<iframe class="item-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.3256424650713!2d106.66413961411655!3d10.78635196196154!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752ecb37e59e33%3A0xfe7c4d9f94f9e079!2zNTkwIEPDoWNoIE3huqFuZyBUaMOhbmcgVMOhbSwgUGjGsOG7nW5nIDExLCBRdeG6rW4gMywgVGjDoG5oIHBo4buRIEjhu5MgQ2jDrSBNaW5oLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1629276436235!5m2!1svi!2s" allowfullscreen="" loading="lazy"></iframe>
					</div>
					<div class="col-md-6 contact-item" style="float: left">
						<button type="submit" class="btn btn-primary"  style="float: right"> Submit</button>
					</div>
				</div>
			</form>
		</div>
	</main>
@endsection