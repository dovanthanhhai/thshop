@extends('frontend.layout.layout')
@section('title',$title ?? '')
@section('content')
    <div class="wrap-show-advance-info-box style-1 has-countdown">
        <div class="wrap-show-advance-info-box style-1 has-countdown">
            <h3 class="title-box">News</h3>
            {{-- <div class="wrap-countdown mercado-countdown" data-expire="2021/9/15 23:59:59"></div> --}}
            <div class="comic-container">
                <div class="comic-product row">
                    @foreach ($blog as $item)
                            <div class="col-lg-2 col-md-2 col-sm-3 col-3 prodductcomic">
                                <div class="item-comic">
                                    <a href="{{ route('product-details',['any'=>$item->url, 'id'=>$item->id]) }}" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                        <figure><img src="{{ asset('/products/' . $item->image_1) }}" width="800" height="800" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                    </a>
                                    <div class="group-flash">
                                        <span class="flash-item new-label">New</span>
                                    </div>
                                    {{-- <div class="wrap-btn">
                                        <a href="#" class="function-link">quick view</a>
                                    </div> --}}
                                </div>
                                <div class="pro-info">
                                    <a href="#" class="product-name"><span>{{ $item->name }}</span></a>
                                    <div class="wrap-price"><span class="product-price">${{ number_format($item->price) }}</span></div>
                                </div>
                            </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection