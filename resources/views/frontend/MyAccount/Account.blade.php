@extends('frontend.layout.layout')
@section('title',$title ?? '')
@section('content')
    <div class="card-body p-0">
        <h1 class="myaccount-1" style="padding-left:20%">Profile</h1>
            <div class="form-horizontal col-lg-8" >
                <div class="information">
                    <div>
                        <p>Username:  {{ $user->name ?? ''}}</p>
                    </div>
                </div>
                <div class="information">
                    <div>
                        <p>
                            Email:  {{ $user->email ?? ''}}
                        </p>
                    </div>
                </div>
                <div class="iinformation">
                    <div>
                        <p>Phone:  {{ $user->phone ?? ''}}</p>
                    </div>
                </div>
                <div class="information">
                    <div>
                        <p>Address:  {{ $user->address?? ''}}</p>
                    </div>
                </div>
                <div class="information">
                    <div>
                        <p>Roles:  {{ $user->roles ?? ''}}</p> 
                    </div>
                </div>
                <div class="infoupdate"><a href="{{ route('customer.edit', Auth::user()->id) }}" style="color: black">Update</a></div>
            </div>
        </div>
    </div>
@endsection
