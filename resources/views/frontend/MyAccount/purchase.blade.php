@extends('frontend.layout.layout')
@section('title',$title ?? '')
@section('content')
    <!--main area-->
	<main id="main" class="main-site">
        <div class="order-item" style="padding: 50px">
            <div class="row">
              <div class="col-md-12 " style="padding: 20px 0px">
                <div class="card-body p-0">
                    <table class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 10%" class="text-center">
                                    Id
                                </th>
                                <th style="width: 10%" class="text-center">
                                    Name
                                </th>
                                <th style="width: 10%" class="text-center">
                                    Phone
                                </th>
                                <th style="width: 10%" class="text-center">
                                    Address
                                </th>
                                <th style="width: 10%" class="text-center">
                                    Email
                                </th>
                                <th style="width: 10%" class="text-center">
                                    Total Price
                                </th>
                                <th style="width: 10%" class="text-center">
                                  Actions
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        @php
                            $stt= 0;
                        @endphp
                        @foreach ($orders as $item)        
                        @php
                            $stt++;
                        @endphp
                            <tr>
                              <td class="text-center">
                                 Order number  {{ $stt }}
                              </td>
                              <td class="text-center">
                                  {{ $item->user_name }}
                              </td>
                              <td class="text-center">
                                  {{ $item->user_phone }}
                              </td>
                              <td class="text-center">
                                  {{ $item->user_address }}
                              </td>
                              <td class="text-center">
                                  {{ $item->user_email }}
                              </td>
                              <td class="text-center">
                                  ${{ number_format($item->total_price) }}
                              </td>
                              <td class="text-center">
                                <a class="btn btn-info btn-sm" href="{{ route('history-detail', $item->id) }}">
                                  See detail
                                </a>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                    </table>
                  </div>
            </div>
        </div>
	</main>
@endsection