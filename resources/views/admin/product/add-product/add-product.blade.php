@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <section class="content">
        <form action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Add Product</h3>
                    </div>
                        <div class="card-body row">
                            <div class="form-group col-md-12">
                                <label for="inputName">Category</label>
                                <select id="inputClientCompany"  name="category_id" class="form-control custom-select">
                                    <option selected disabled>Select one</option>
                                    @if (!empty ($categorys))
                                        @foreach ($categorys as $c)
                                            <option value="{{ $c->id }}">{{ $c->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="inputName">Name</label>
                                <input type="text" id="inputClientCompany" name="name" required class="form-control">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="inputClientCompany">Author</label>
                                <input type="text" id="inputClientCompany" name="author" required class="form-control">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="inputClientCompany">Price</label>
                                <input type="text" id="inputClientCompany" name="price" required class="form-control">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="inputClientCompany">Price Sale</label>
                                <input type="text" id="inputClientCompany" name="price_sale"  class="form-control">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="inputClientCompany">Description</label>
                                <textarea id="message" class="form-control" name="description" required></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="inputClientCompany">Publisher Company</label>
                                <input type="text" id="inputClientCompany" name="pub_company"  class="form-control">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="inputClientCompany">Publication Date</label>
                                <input type="text" id="inputClientCompany" name="date_publishing"  class="form-control">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="inputClientCompany">Publisher</label>
                                <input type="text" id="inputClientCompany" name="publisher" class="form-control">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="inputClientCompany">Status</label>
                                <input type="text" id="inputClientCompany" name="status" required class="form-control">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="inputClientCompany">Url</label>
                                <input type="text" id="inputClientCompany" name="url" required class="form-control">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="image">Image</label>
                                <input type="file" id="image" name="image_1" required class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ route('product.index') }}" class="btn btn-secondary">Cancel</a>
                    <input type="submit" value="Create" class="btn btn-success float-right">
                </div>
            </div>
        </form>
    </section>
@endsection