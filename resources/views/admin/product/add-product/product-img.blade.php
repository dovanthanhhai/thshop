@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <section class="content">
        <form action="{{ route('product-img.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Add Product Image</h3>
                    </div>
                        <div class="card-body row">
                            <div class="form-group col-md-8">
                                <label for="inputName">Product</label>
                                <select id="inputClientCompany"  name="product_id" class="form-control custom-select">
                                    <option selected disabled>Select one</option>
                                    @if (!empty ($product))
                                        @foreach ($product as $c)
                                            <option value="{{ $c->id }}">{{ $c->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-8">
                                <label for="image">Image</label>
                                <input type="file" id="image" name="image" required class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <a href="#" class="btn btn-secondary">Cancel</a>
                    <input type="submit" value="Create" class="btn btn-success float-right">
                </div>
            </div>
        </form>
    </section>
@endsection