@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <section class="content">
        <form action="{{ route('product.update',$products->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Add Product</h3>
                    </div>
                        <div class="card-body row">
                            <div class="form-group col-md-6">
                                <label for="inputName">Category</label>
                                <select id="inputClientCompany"  name="category_id"  class="form-control custom-select">
                                    <option selected disabled>{{ $products->category_id }}</option>
                                    @if (!empty ($categorys))
                                        @foreach ($categorys as $c)
                                            <option value="{{ $c->id }}">{{ $c->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputName">Name</label>
                                <input type="text" id="inputClientCompany" name="name" value="{{ $products->name }}" required class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany">Author</label>
                                <input type="text" id="inputClientCompany" name="author" value="{{ $products->author }}"  required class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany">Price</label>
                                <input type="text" id="inputClientCompany" name="price" value="{{ $products->price }}"  required class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany">Price Sale</label>
                                <input type="text" id="inputClientCompany" name="price_sale" value="{{ $products->price_sale }}"   class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany">Description</label>
                                <input type="text" id="inputClientCompany" name="description" value="{{ $products->description }}"  required class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany">Publisher Company</label>
                                <input type="text" id="inputClientCompany" name="pub_company" value="{{ $products->pub_company }}"   class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany">Publication Date</label>
                                <input type="text" id="inputClientCompany" name="date_publishing"  value="{{ $products->date_publishing }}"  class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany">Publisher</label>
                                <input type="text" id="inputClientCompany" name="publisher" value="{{ $products->publisher }}" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany">Status</label>
                                <input type="text" id="inputClientCompany" name="status" value="{{ $products->status }}" required class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany">Url</label>
                                <input type="text" id="inputClientCompany" name="url" value="{{ $products->url }}" required class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="image">Image</label>
                                <input type="file" id="image" name="image_1" value="{{ $products->image_1 }}" required class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ route('product.index') }}" class="btn btn-secondary">Cancel</a>
                    <input type="submit" value="Create" class="btn btn-success float-right">
                </div>
            </div>
        </form>
    </section>
@endsection