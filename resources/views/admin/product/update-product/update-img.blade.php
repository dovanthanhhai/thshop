@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <section class="content">
        <form action="{{ route('product-img.update',$pro_img->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Update Image</h3>
                    </div>
                        <div class="card-body row">
                            <div class="form-group col-md-12">
                                <label for="inputName">Product</label>
                                <select id="inputClientCompany"  name="product_id" class="form-control custom-select">
                                    <option selected disabled>{{ $pro_img->Prod->name ??''}}</option>
                                    @if (!empty ($product))
                                        @foreach ($product as $c)
                                            <option value="{{ $c->id }}">{{ $c->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="image">Image</label>
                                <input type="file" id="image" name="image" value="{{ $pro_img->image }}" required class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a href="#" class="btn btn-secondary">Cancel</a>
                    <input type="submit" value="Upsate" class="btn btn-success float-right">
                </div>
            </div>
        </form>
    </section>
@endsection