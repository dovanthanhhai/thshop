@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Detail Customer</h3>
  
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card">
            <div class="card-body" style="padding-left:40px">
                <div class="row">
                    <div class="wrap-input col-md-12">
                        <label for="frm-reg-lname">Id: {{ $users->id }}</label>
                    </div>
                    <div class="wrap-input col-md-12">
                        <label for="frm-reg-lname">Username: {{ $users->username }}</label>
                    </div>
                    <div class="wrap-input col-md-12">
                        <label for="frm-reg-lname">Name: {{ $users->name }}</label>
                    </div>
                    <div class="wrap-input col-md-12">
                        <label for="frm-reg-lname">Gender: {{ $users->gender }}</label>
                    </div>
                    <div class="wrap-input col-md-12">
                        <label for="frm-reg-lname">Address: {{ $users->address }}</label>
                    </div>
                    <div class="wrap-input col-md-12">
                        <label for="frm-reg-lname">Phone: {{ $users->phone }}</label>
                    </div>
                    <div class="wrap-input col-md-12">
                        <label for="frm-reg-lname">Email: {{ $users->email }}</label>
                    </div>
                    <div class="wrap-input col-md-12">
                        <label for="frm-reg-lname">Status: {{ $users->status }}</label>
                    </div>
                    <div class="wrap-input col-md-12">
                        <label for="frm-reg-lname">Roles: {{ $users->roles }}</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
    