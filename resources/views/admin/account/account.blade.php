@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Customer</h3>
  
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 3%" class="text-center">
                          Id
                      </th>
                      <th style="width: 10%" class="text-center">
                          Name
                      </th>
                      <th style="width: 10%" class="text-center">
                          Email
                      </th>
                      <th style="width: 10%" class="text-center">
                          Phone
                      </th>
                      <th style="width: 10%" class="text-center">
                          Address
                      </th>
                      <th style="width: 10%" class="text-center">
                          Gender
                      </th>
                      <th style="width: 10%" class="text-center">
                          Status
                      </th>
                      <th style="width: 10%" class="text-center">
                          Roles
                      </th>
                      <th style="width: 10%" class="text-center">
                        Actions
                      </th>
                  </tr>
              </thead>
              <tbody>
                @foreach ($users as $item)
                    <tr>
                        <td class="text-center">
                            {{ $item->id??'' }}
                        </td>
                        <td class="text-center">
                            {{ $item->name }}
                        </td>
                        <td class="text-center">
                            {{ $item->email }}
                        </td>
                        <td class="text-center">
                            {{ $item->phone }}
                        </td>
                        <td class="text-center">
                            {{ $item->address }}
                        </td>
                        <td class="text-center">
                            {{ $item->gender }}
                        </td>
                        <td class="text-center">
                            {{ $item->status }}
                        </td>
                        <td class="text-center">
                            {{ $item->roles }}
                        </td>
                        <td class="project-actions text-center">
                            <a class="btn btn-info btn-sm" href="{{ route('detail-customer', $item->id) }}">
                                See detail
                            </a>
                            {{-- <a class="btn btn-danger btn-sm" href="#">
                                <i class="fas fa-trash">
                                </i>
                            </a> --}}
                        </td>
                    </tr>
                @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection
    