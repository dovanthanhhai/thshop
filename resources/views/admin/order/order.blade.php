@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Order</h3>
  
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 3%" class="text-center">
                          Id
                      </th>
                      <th style="width: 10%" class="text-center">
                          Name
                      </th>
                      <th style="width: 10%" class="text-center">
                          Phone
                      </th>
                      <th style="width: 10%" class="text-center">
                          Address
                      </th>
                      <th style="width: 10%" class="text-center">
                          Email
                      </th>
                      <th style="width: 10%" class="text-center">
                          Total Price
                      </th>
                      {{-- <th style="width: 10%" class="text-center">
                        Status Payment
                      </th> --}}
                      <th style="width: 10%" class="text-center">
                        Actions
                      </th>
                  </tr>
              </thead>
              <tbody>
                @foreach ($orders as $item)
                  <tr>
                    <td class="text-center">
                        {{ $item->id }}
                    </td>
                    <td class="text-center">
                        {{ $item->user_name }}
                    </td>
                    <td class="text-center">
                        {{ $item->user_phone }}
                    </td>
                    <td class="text-center">
                        {{ $item->user_address }}
                    </td>
                    <td class="text-center">
                        {{ $item->user_email }}
                    </td>
                    <td class="text-center">
                        ${{ number_format($item->total_price) }}
                    </td>
                    <td class="text-center">
                      <a class="btn btn-info btn-sm" href="{{ route('detail-order', $item->id) }}">
                        See detail
                      </a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection