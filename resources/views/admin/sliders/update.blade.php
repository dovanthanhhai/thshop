@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <section class="content">
        <form action="{{ route('sliders.update',$slider->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Slider Website</h3>
                    </div>
                        <div class="card-body row">
                            <div class="form-group col-md-6">
                                <label for="image">Image</label>
                                <input type="file" id="image" name="image" value="{{ $slider->image }}" required class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany">Url</label>
                                <input type="url" id="inputClientCompany" name="url" value="{{ $slider->url }}" required class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany"></label>
                                <input type="date" id="inputClientCompany" name="created_by" value="{{ $slider->created_by }}" required class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ route('sliders.index') }}" class="btn btn-secondary">Cancel</a>
                    <input type="submit" value="Create" class="btn btn-success float-right">
                </div>
            </div>
        </form>
    </section>
@endsection