@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <section class="content">
        <form action="{{ route('config.update',$config->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Dislpay Website</h3>
                    </div>
                        <div class="card-body row">
                            <div class="form-group col-md-6">
                                <label for="inputName">Website Name</label>
                                <input type="text" id="inputClientCompany" name="webname" value="{{ $config->webname }}" required class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="image">Logo</label>
                                <input type="file" id="image" name="logo" value="{{ $config->logo }}" required class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany">Hotline</label>
                                <input type="text" id="inputClientCompany" name="hotline" value="{{ $config->hotline }}" required class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany">Facebook</label>
                                <input type="text" id="inputClientCompany" name="facebook" value="{{ $config->facebook }}" required class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany">Zalo</label>
                                <input type="text" id="inputClientCompany" name="zalo" value="{{ $config->zalo }}" required class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany">Address</label>
                                <input type="text" id="inputClientCompany" name="address" value="{{ $config->address }}" required class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputClientCompany">Email</label>
                                <input type="email" id="inputClientCompany" name="email" value="{{ $config->email }}" required class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ route('config.index') }}" class="btn btn-secondary">Cancel</a>
                    <input type="submit" value="Update" class="btn btn-success float-right">
                </div>
            </div>
        </form>
    </section>
@endsection