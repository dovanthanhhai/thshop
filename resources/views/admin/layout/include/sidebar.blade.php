  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset ('/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          @foreach ($config as $item)
            <a href="{{ route('index') }}" class="d-block">{{ $item->webname ?? ''}}</a>
          @endforeach
          <a href="{{ route('account.edit', Auth::user()->id) }}" class="d-block"><span style="font-size: 12px">Edit information</span></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('dashboard') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-bible"></i>
              <p>
                Product manage 
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('product.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Products</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('product-img.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Product Image</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('product-cmt.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Product Comment</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('accountAdmin') }}" class="nav-link">
              <i class="nav-icon far fa-user"></i>
              <p>
                Account Admin
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('account.index') }}" class="nav-link">
              <i class="nav-icon far fa-user"></i>
              <p>
                Customer List
              </p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="{{route('orders.index')}}" class="nav-link">
              <i class="nav-icon fab fa-opencart"></i>
              <p>
                Order
                <span class="badge badge-info right">{{ $order ?? ''}}</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('blogadmin.index') }}" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Blog
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('contact') }}" class="nav-link">
              <i class="nav-icon far fa-id-card"></i>
              <p>
                Contact
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="" class="nav-link">
              <i class=" nav-icon fas fa-sliders-h"></i>
              <p>
                Slider manage
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('sliders.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p> Sliders</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('sliders.create') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Sliders</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('config.index') }}" class="nav-link">
              <i class=" nav-icon fas fa-cog"></i>
              <p>
                Config
                {{-- <i class="fas fa-angle-left right"></i> --}}
              </p>
            </a>
            {{-- <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('config.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Display Website</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('config.create') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Display Website</p>
                </a>
              </li>
            </ul> --}}
          </li>
            <div href="{{ route('config.index') }}" class="nav-link">
              <p>
                <div style="float: left"> 
                  <form action="{{ route('admin.logout') }}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-primary">Logout</button>
                  </form>
                </div>
              </p>
            </div>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  
