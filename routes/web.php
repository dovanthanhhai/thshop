<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-session', function() {
    session()->forget('username');
    // return back();
});

Route::get('/','frontend\HomeController@index')->name('index');
Route::get('search','frontend\HomeController@search')->name('search');
//                         LOGIN                 //
Route::resource('login','frontend\LoginController');  //frontend
Route::post('logout','frontend\LoginController@logout')->name('logout');//frontend
Route::get('/admin/login','backend\HomeController@loginAdmin')->name('admin.login'); //backend
Route::post('process-login','backend\HomeController@processLogin')->name('process.login'); //backend
Route::post('admin-logout','backend\HomeController@logout')->name('admin.logout');//backend
///////////////////////////////////////////////////////////////////////////////////////////////////////
                //          frontend         //
Route::resource('user','frontend\UserController');
Route::get('/order-create','backend\OrderController@create')->name('order-create');
Route::post('/orders-store','backend\OrderController@store')->name('orders-store');
Route::get('/contactus','frontend\HomeController@contactus')->name('contactus');
Route::get('/detail','frontend\HomeController@detail')->name('detail');
Route::get('/checkout','frontend\HomeController@checkout')->name('checkout');
Route::get('/aboutus','frontend\HomeController@aboutus')->name('aboutus');
Route::get('/comic','frontend\HomeController@comic')->name('comic');
Route::get('/stationery','frontend\HomeController@stationery')->name('stationery');
Route::get('/magazine','frontend\HomeController@magazine')->name('magazine');
Route::get('/promotion','frontend\HomeController@promotion')->name('promotion');
Route::get('/book','frontend\HomeController@book')->name('book');
Route::get('/Faq','frontend\HomeController@Faq')->name('Faq');
Route::get('/my-account','frontend\MyAccountController@MyAccount')->name('my-account');
Route::get('/purchase','frontend\MyAccountController@purchase')->name('purchase');
Route::resource('product-img','backend\ProductImgController');
Route::resource('product-cmt','backend\ProductCmtController'); 
Route::resource('customer','frontend\UserController');
Route::get('/blog','frontend\HomeController@blog')->name('blog');
Route::get('/history','frontend\HomeController@history')->name('history');
Route::get('/history-detail/{id}','frontend\HomeController@histotyDeitail')->name('history-detail');
Route::resource('contact','frontend\ContactController');
//                       cart                       //
Route::get('/cart','frontend\CartController@cart')->name('cart');
Route::post('/add-cart','frontend\CartController@addcart')->name('add.cart');
Route::post('/delete-cart-item', 'frontend\CartController@deleteCartItem')->name('delete-cart-item');
Route::post('/delete-cart', 'frontend\CartController@deleteCart')->name('delete-cart');
Route::post('/chang-cart-quantity', 'frontend\CartController@changCartQuantity')->name('chang-cart-quantity');
/////////////////////////////////////////////////////////////////////////////////////////////////////
                 //          backend             //       
Route::group(['prefix' => 'admin',  'middleware' => 'admin'], function() {
    Route::get('/','backend\HomeController@dashboard')->name('dashboard');
    Route::get('/accountAdmin','backend\HomeController@adminIndex')->name('accountAdmin');
    Route::resource('account','backend\UserController');
    Route::resource('config','backend\ConfigController');
    Route::resource('sliders','backend\SliderController');
    Route::resource('product','backend\ProductController');
    Route::resource('orders','backend\OrderController');
    Route::resource('order-detail','backend\OrderDetailController');
    Route::get('order/{id}','backend\OrderController@orderDetail')->name('detail');
    Route::get('/detail-customer/{id}','backend\HomeController@detailCustomer')->name('detail-customer');
    Route::get('/detail/{id}','backend\HomeController@OrderDetail')->name('detail-order');
    Route::resource('blogadmin','backend\BlogController');
    Route::get('/contact','backend\HomeController@contact')->name('contact');
});
Route::get('{any}-{id}','frontend\HomeController@productDetails')->name('product-details');