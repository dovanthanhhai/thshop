<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    public $id;
    public $name;
    public $quantity;
    public $price;
    public $image;
    public $url;

    //khai báo dựng
    public function __construct ($data_add){
        $this->id = $data_add['id'] ?? 0;
        $this->name = $data_add['name'] ?? '';
        $this->quantity = $data_add['qty'] ?? 0;
        $this->price = $data_add['price'] ?? 0;
        $this->image = $data_add['image'] ?? '';
        $this->url = $data_add['url'] ?? '';
    }
}
