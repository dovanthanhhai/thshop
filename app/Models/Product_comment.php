<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product_comment extends Model
{
    use HasFactory;
    protected $fillable = [
        'product_id',
        'users_id',
        'content',
        'rate'
    ];
    public function Username(){
        return $this->hasOne(User::class, 'id', 'users_id');
    }
    public function Product(){
        return $this->hasOne(Products::class, 'id', 'product_id');
    }
}
