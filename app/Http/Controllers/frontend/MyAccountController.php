<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Config;
use App\Models\Order_detail;
use App\Models\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class MyAccountController extends Controller
{
    public function MyAccount(Request $request){
        
        $login = Auth::user();
        $config = Config::all();
        $user = Auth::user();
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $cart_number = count($cart);
        $title = 'MyAccount';
        return view('frontend.MyAccount.Account',['title' => $title],compact('config','cart_number','user','login'));
    }
    
    // public function purchase(){
    //     $orderDetail = Order_detail::all();
    //     $order = Orders::where('user_id', Auth::user()->id)->count('id');
    //     $title = 'Admin | Order Detail';
    //     return view ('frontend.MyAccount.purchase',['title' => $title], compact('orderDetail','order'));
    // }
    public function processUpdateAccount(Request $request){
        $user = User::all();
        $user->username = $request->input('username');
        $user->name = $request->input('name');
        $user->gender = $request->input('gender');
        $user->address = $request->input('address');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->save();

        Session::flash('message', "Successfully updated");
        return redirect()->route('my-account');
    }

}