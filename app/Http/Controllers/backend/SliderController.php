<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Config;
use App\Models\Orders;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = Config::all();
        $order = Orders::all()->count('id');
        $slider = Slider::all();
        $title = 'Admin | Slider';
        return view('admin.sliders.index' , ['title' => $title],compact('slider','order','config'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $config = Config::all();
        $order = Orders::all()->count('id');
        $title = 'Admin | Slider';
        return view('admin.sliders.slider', ['title' => $title],compact('order','config'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slider = $request->all();
        // kiểm tra file có tồn tại hay không?
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            // lấy phần mở rộng (extension) của file để kiểm tra xem 
            // đây có phải là file hình
            $extension = $file->getClientOriginalExtension();            
            if ($extension != 'jpg' && $extension != 'jpeg' && $extension != 'png') {
                return redirect()->route('sliders.slider');
            }
            
            $imgName = $file->getClientOriginalName();
            // copy file vào thư mục public/images
            $file->move('slider', $imgName);
            // tạo phần tử image trong mảng $product
            $slider['image'] = $imgName;
        }
        Slider::create($slider);
        return redirect()->route('admin.sliders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $config = Config::all();
        $slider = Slider::find($id);
        $title = 'Admin | Slider';
        return view('admin.sliders.update', ['title' => $title],compact('slider','config'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $slider = Slider::find($id);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();            
            if ($extension != 'jpg' && $extension != 'jpeg' && $extension != 'png') {
                return redirect()->route('sliders.slider');
            }
            
            $imgName = $file->getClientOriginalName();
            $file->move('slider', $imgName);
            $slider['image'] = $imgName;
        }
        $slider->url = $request->input('url');
        $slider->created_by = $request->input('created_by');
        $slider->save();
        return redirect()->route('sliders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        if ($slider->image != null) {
            if (file_exists('slider/' . $slider->image)) {
                unlink('slider/' . $slider->image);
            }
        }
        $slider->delete();
        return redirect()->route('sliders.index');
    }
}
