<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Config;
use App\Models\Orders;
use App\Models\Product_image;
use App\Models\Products;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProductImgController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = Config::all();
        $product_img = Product_image::all();
        $order = Orders::all()->count('id');
        $title = 'Admin | Product Image';
        return view ('admin.product.product-img',['title' =>$title], compact('product_img','order','config'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $config = Config::all();
        $order = Orders::all()->count('id');
        $title = 'Admin | Add Image';
        return view('admin.product.add-product.product-img' ,['title' =>$title], compact('order','config'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product_img = new Product_image();
        $product_img->product_id = $request->product_id;
        $product_img->image = $request->image;
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            // lấy phần mở rộng (extension) của file để kiểm tra xem 
            // đây có phải là file hình
            $extension = $file->getClientOriginalExtension();            
            if ($extension != 'jpg' && $extension != 'jpeg' && $extension != 'png') {
                return redirect()->route('product.add-product.product-img');
            }
            
            $imgName = $file->getClientOriginalName();
            // copy file vào thư mục public/images
            $file->move('productImg', $imgName);
            // tạo phần tử image trong mảng $product
            $product_img['image'] = $imgName;
        }
        // Product_image::create($product_img);
        $product_img->save();
        return redirect()->route('product-img.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product_image  $product_image
     * @return \Illuminate\Http\Response
     */
    public function show(Product_image $product_image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product_image  $product_image
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pro_img = Product_image::find($id);
        $product =  Products::all();
        $config = Config::all();
        $order = Orders::all()->count('id');
        $title = 'Admin | Edit image';
        return view('admin.product.update-product.update-img' , ['title' => $title],compact('pro_img','order','config','product',));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product_image  $product_image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $pro_img = Product_image::find($id);
        $pro_img->product_id = $request->input('product_id');
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            // lấy phần mở rộng (extension) của file để kiểm tra xem 
            // đây có phải là file hình
            $extension = $file->getClientOriginalExtension();            
            if ($extension != 'jpg' && $extension != 'jpeg' && $extension != 'png') {
                return redirect()->route('product.add-product.product-img');
            }
            
            $imgName = $file->getClientOriginalName();
            // copy file vào thư mục public/images
            $file->move('productImg', $imgName);
            // tạo phần tử image trong mảng $product
            $pro_img['image'] = $imgName;
        }
        $pro_img->save();
        Session::flash('message', "Successfully deleted");
        return redirect()->route('product-img.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product_image  $product_image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product_image $product_image)
    {
        if ($product_image->image != null) {
            if (file_exists('productImg/' . $product_image->image)) {
                unlink('productImg/' . $product_image->image);
            }
        }
        $product_image->delete();
        return redirect()->route('product-img.index');
    }
}
