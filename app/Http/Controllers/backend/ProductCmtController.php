<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Config;
use App\Models\Orders;
use App\Models\Product_comment;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductCmtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = Config::all();
        $product_cmt = Product_comment::all()->sortDesc();
        $products = Products::all();
        $order = Orders::all()->count('id');
        $title = 'Admin | Product Comment';
        return view ('admin.product.product-cmt',['title' =>$title], compact('product_cmt','order','products','config'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cmt = $request->all();
        $cmt['users_id'] = Auth::user()->id;
        Product_comment::create($cmt);
        return redirect( $request->url);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product_comment  $product_comment
     * @return \Illuminate\Http\Response
     */
    public function show(Product_comment $product_comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product_comment  $product_comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Product_comment $product_comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product_comment  $product_comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product_comment $product_comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product_comment  $product_comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product_comment $product_comment)
    {
        $product_comment->delete();
        return redirect()->route('product-cmt.index');
    }
}
