<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Config;
use App\Models\Order_detail;
use App\Models\Orders;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = Config::all();
        $orders = Order_detail::where('product_id','!=','0');
        // $products = Products::all()->sortByDesc('id');
        $products = DB::table('Products')->select("products.*",DB::raw('(CASE WHEN order_details.id IS NOT NULL THEN "0" ELSE 1 END) AS delete_id'))
                                        ->leftJoin('order_details','products.id','=','order_details.product_id')
                                        ->orderBy('products.id','desc')->groupBy('products.id')->get();
        $order = Orders::all()->count('id');
        $title = 'Book Store | Products';
        return view('admin.product.index',['title' => $title],compact('products','order','orders','config'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $config = Config::all();
        $categorys =  Category::all();
        $title = 'Book Store | Create Products';
        $order = Orders::all()->count('id');
        return view('admin.product.add-product.add-product',['title' => $title], compact('categorys','order','config'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $products = $request->all();
        // kiểm tra file có tồn tại hay không?
        if ($request->hasFile('image_1')) {
            $file = $request->file('image_1');
            // lấy phần mở rộng (extension) của file để kiểm tra xem 
            // đây có phải là file hình
            $extension = $file->getClientOriginalExtension();            
            if ($extension != 'jpg' && $extension != 'jpeg' && $extension != 'png') {
                return redirect()->route('product.add-product');
            }
            
            $imgName = $file->getClientOriginalName();
            // copy file vào thư mục public/images
            $file->move('products', $imgName);
            // tạo phần tử image trong mảng $product
            $products['image_1'] = $imgName;
        }
        Products::create($products);
        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show(Products $products)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $config = Config::all();
        $categorys =  Category::all();
        $products = Products::find($id);
        $title = 'Book Store | Edit Products';
        return view('admin.product.update-product.update-product',['title' => $title],compact('products','categorys','config'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $products = Products::find($id);
        $products->category_id = $request->input('category_id');
        $products->name = $request->input('name');
        $products->author = $request->input('author');
        $products->price = $request->input('price');
        $products->price_sale = $request->input('price_sale');
        $products->description = $request->input('description');
        $products->pub_company = $request->input('pub_company');
        $products->date_publishing = $request->input('date_publishing');
        $products->publisher = $request->input('publisher');
        $products->status = $request->input('status');
        $products->url = $request->input('url');
        if ($request->hasFile('image_1')) {
            $file = $request->file('image_1');
            // lấy phần mở rộng (extension) của file để kiểm tra xem 
            // đây có phải là file hình
            $extension = $file->getClientOriginalExtension();            
            if ($extension != 'jpg' && $extension != 'jpeg' && $extension != 'png') {
                return redirect()->route('product.add-product');
            }
            
            $imgName = $file->getClientOriginalName();
            // copy file vào thư mục public/images
            $file->move('products', $imgName);
            // tạo phần tử image trong mảng $product
            $products['image_1'] = $imgName;
        }
        $products->save();
        Session::flash('message', "Successfully deleted");
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(Products $products,$id)
    {   
        $products = Products::find($id);
        if ($products->image_1 != null) {
            if (file_exists('products/' . $products->image_1)) {
                unlink('products/' . $products->image_1);
            }
        }
        $products->delete();
        return redirect()->route('product.index');
    }
}
